@extends('layout')
@section('content')
    @include('nav')
    <a href="#" onclick="history.go(-1)" class="backToIssuesLink"> &#8634; Back to Issues</a>
    <div class="entryContainer">
        <div class="issueTitleAndInformation">
            <div class="title">{{ $issue->title }} <span class="entryNumber">#{{ $issue->id }}</span></div>
            <div class="issueStatus">
                <img class="exclamationwhite" src="/img/exclamationwhite.png">
                <span class="issueState">{{ strtoupper($issue->state) }}</span>
            </div>
            <div class="entryInformation"><span class="entryOwner">{{ $issue->user->login }} </span>opened this issue
                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($issue->created_at))->diffForHumans() }}
                <span>- {{ count($entries) }} comments</span>
            </div>
        </div>
        @foreach($entries as $entry)
            <div class="entry">
                <div class="avatar">
                    <img src="{{ $entry->user->avatar_url }}"/>
                </div>
                <div class="entryHeadLine">
                    <span class="entryOwner">{{$entry->user->login}}</span>
                    <span> commented on {{ \Carbon\Carbon::createFromTimeStamp(strtotime($entry->created_at))->diffForHumans() }}</span>
                </div>
                <div class="entryContent">{{ $entry->body }}</div>
            </div>
        @endforeach
    </div>
@endsection