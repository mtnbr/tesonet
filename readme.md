#Tesonet Task
Web application with github auth with Laravel and React

## Installation

- Check server requirements below
- Pull this repository to your machine
- Go to path-to-your-project

```
composer install
```
- Create a database for your app
- Change name of your env example file with .env
```
mv .env.example .env
```
- Fix the lines of .env file for DB_DATABASE, DB_USERNAME, DB_PASSWORD, DB_CONNECTION

- Add file permission
```
sudo chown -R {YOUR USERNAME}:www-data /path-to-your-project/vendor
sudo chown -R {YOUR USERNAME}:www-data /path-to-your-project/storage
```

- Run the command below at the path of your project

```
php artisan migrate
```

### Server Requirements

- Composer
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension