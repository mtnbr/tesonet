@extends('layout')
@section('content')
    @include('nav')
    <article>
        <div id="listOfIssues"
             data-csrf_token="{{ csrf_token() }}"
             data-issues="{{ json_encode($issues->items()) }}"
             data-count="{{ json_encode($issueStateCounts) }}">
        </div>
        <div class="bridge">
            <h1>Full Stack Developer Task</h1>by
            <div class="inlineAppName">trakaio</span><span>.</span></div>
        </div>
    </article>
    <div class="paginations">{{ $issues->links('pagination', ['issues' => $issues]) }}</div>
@endsection