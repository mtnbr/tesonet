<?php

namespace App\Http\Controllers;

use App\Helpers\IssuePaginator;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Helpers\IssueStateCounter;
use Illuminate\Support\Facades\Auth;

class GithubApiController extends Controller
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var IssuePaginator
     */
    protected $issuePaginator;

    /**
     * GithubApiController constructor.
     * @param Client $client
     * @param IssuePaginator $issuePaginator
     */
    public function __construct(Client $client, IssuePaginator $issuePaginator)
    {
        $this->client = $client;
        $this->issuePaginator = $issuePaginator;
        Auth::loginUsingId(1);
    }

    /**
     * @param Request $request
     * @param string $state
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getAllIssues(Request $request, string $state = 'all')
    {
        $uri = 'https://api.github.com/repos/twbs/bootstrap/issues?state=all&filter=' .
            $state . '&access_token=' . $request->user()->token .
            '&per_page=100';

        try {
            $response = $this->client->request('GET', $uri);
        } catch (ClientException $e) {
            return redirect('/logout')->with(
                'exceptionGithub',
                $e->getMessage()
            );
        }

        $issues = json_decode($response->getBody());

        $issueStateCounts = IssueStateCounter::countIssueStates($issues);

        $paginatedItems = $this->issuePaginator->paginateIssues(
            $issues,
            $request->url()
        );

        return view('list', [
            'issues'           => $paginatedItems,
            'issueStateCounts' => $issueStateCounts
        ]);
    }

    /**
     * @param Request $request
     * @param int $issueId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEntry(Request $request, int $issueId)
    {
        $uri = 'https://api.github.com/repos/twbs/bootstrap/issues/' .
            $issueId . '/comments?access_token=' . $request->user()->token;

        try {
            $response = $this->client->request('GET', $uri);
        } catch (ClientException $e) {
            return redirect('/logout')->with(
                'exceptionGithub',
                $e->getMessage()
            );
        }

        $entries = json_decode($response->getBody());
        $issue = json_decode($request->get('issue'));

        return view('entry', ['entries' => $entries, 'issue' => $issue]);
    }
}
