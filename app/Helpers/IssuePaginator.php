<?php

namespace App\Helpers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class IssuePaginator
{
    const ISSUE_LIST_LIMIT_PER_PAGE = 35;

    /**
     * @param array $issues
     * @param string $url
     * @return LengthAwarePaginator
     */
    public function paginateIssues(array $issues, string $url)
    {
        $issues = collect($issues);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageItems = $this->sliceItems($currentPage, $issues);

        $paginatedItems = $this->paginateItems(
            $currentPageItems,
            $issues
        );

        $paginatedItems->setPath($url);

        return $paginatedItems;
    }

    /**
     * @param int $currentPage
     * @param Collection $issues
     * @return array
     */
    public function sliceItems(int $currentPage, Collection $issues)
    {
        $offset = ($currentPage * self::ISSUE_LIST_LIMIT_PER_PAGE) -
            self::ISSUE_LIST_LIMIT_PER_PAGE;

        return $issues->slice(
            $offset,
            self::ISSUE_LIST_LIMIT_PER_PAGE
        )->all();
    }

    /**
     * @param array $currentPageItems
     * @param Collection $issues
     * @return LengthAwarePaginator
     */
    public function paginateItems(array $currentPageItems, Collection $issues)
    {
        return new LengthAwarePaginator(
            $currentPageItems,
            count($issues),
            self::ISSUE_LIST_LIMIT_PER_PAGE
        );
    }
}
