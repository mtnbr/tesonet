<?php
//Login & Logout Page For The App
Route::view('/', 'login')->name('login');
Route::any('/logout', 'Auth\LogoutController@logout')->name('logout');

//Github Auth
Route::get('/auth/github', 'Auth\GithubAuthController@redirectToProvider');
Route::get('/auth/github/callback', 'Auth\GithubAuthController@handleProviderCallback');

//List of Issues For Bootstrap.js
Route::get('/list/{userId}/{token}', 'GithubApiController@getAllIssues')->name('list');

//Entry
Route::post('/issues/{issueId}', 'GithubApiController@getEntry')->name('entry');