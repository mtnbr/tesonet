<?php

namespace App\Helpers;

class IssueStateCounter
{
    /**
     * @param array $issues
     * @return array
     */
    public static function countIssueStates ( $issues )
    {
        $closed = 0;
        $open = 0;

        foreach ( $issues as $issue ) {
            if ( $issue->state === 'closed' ) {
                $closed++;
            }
            if ( $issue->state === 'open' ) {
                $open++;
            }
        }

        return [ 'open' => $open, 'closed' => $closed ];
    }
}