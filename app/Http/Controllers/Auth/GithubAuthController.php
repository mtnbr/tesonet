<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Socialite;
use Redirect;

class GithubAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('github')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/github');
        }

        $authUser = $this->findOrCreateUser($user);
        $this->updateValidToken($authUser, $user->token);

        Auth::login($authUser, true);

        return Redirect::to('/list/' . $user->id . '/' . $user->token);
    }

    /**
     * Return user if exists; create and return if doesn't
     * @param $githubUser
     * @return User
     */
    private function findOrCreateUser($githubUser)
    {
        if ($authUser = User::where('github_id', $githubUser->id)->first()) {
            return $authUser;
        }

        return User::create([
            'name'      => $githubUser->nickname,
            'email'     => $githubUser->email,
            'github_id' => $githubUser->id,
            'avatar'    => $githubUser->avatar,
            'token'     => $githubUser->token
        ]);
    }

    /**
     * @param User $authUser
     * @param $token
     */
    public function updateValidToken($authUser, $token)
    {
        if ($authUser->token != $token) {
            $authUser->update(['token' => $token]);
        }
    }
}
