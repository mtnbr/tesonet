import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.LoginWithGithub = this.LoginWithGithub.bind(this);
    }

    LoginWithGithub() {
      window.location.href ="/auth/github"
    }

    render() {
        return (
            <div className="background">
                <div className="itemsLogin">
                    <div className="loginTitle">trakaio<span>.</span></div>
                    <button type="button" className="loginButton" onClick={this.LoginWithGithub}>Login With Github</button>
                </div>
            </div>
        );
    }
}

if (document.getElementById('login')) {
    ReactDOM.render(<Login/>, document.getElementById('login'));
}
