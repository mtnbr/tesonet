import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TimeAgo from 'react-timeago';

export default class ListOfIssues extends Component {
    constructor(props) {
        super(props);
        this.changeIssuesFilter = this.changeIssuesFilter.bind(this);
        this.state = {
            count:  JSON.parse(this.props.count) || [],
            issues: JSON.parse(this.props.issues) || [],
            issueFilter: sessionStorage.getItem('filter') || 'block',
            iconPath : '/img/all.png'
        }
    }

    changeIssuesFilter(filter, iconPath) {
        sessionStorage.setItem('filter', filter)
        this.setState({
            issueFilter: filter,
            iconPath: iconPath
        });
    }

    getIssueFilter(stateOfIssueFilter, issueState) {
        if (stateOfIssueFilter === 'closed' && issueState !== 'closed') {
            return 'none'
        }
        if (stateOfIssueFilter === 'open' && issueState !== 'open') {
            return 'none'
        }
        if (stateOfIssueFilter === 'block') {
            return 'block'
        }
    }

    render() {
        return (
            <div className="issues">
                <div className="issueFilters">
                    <div className="closedFilterButton pointer" onClick={() => this.changeIssuesFilter('block','/img/all.png')}>
                        <img className="check" src="/img/all.png"/>
                        <span className="textOfIssues">{this.state.count.closed + this.state.count.open} All</span>
                    </div>
                    <div className="openFilterButton pointer" onClick={() => this.changeIssuesFilter('open','/img/exclamationgreen.png')}>
                        <img className="exclamation" src="/img/exclamation.png"/>
                        <span className="textOfIssues">{this.state.count.open} Open</span>
                    </div>
                    <div className="closedFilterButton pointer" onClick={() => this.changeIssuesFilter('closed','/img/check.png')}>
                        <img className="check" src="/img/check.png"/>
                        <span className="textOfIssues">{this.state.count.closed} Closed</span>
                    </div>
                </div>
                {Object.keys(this.state.issues).map(key => this.state.issues[key]).map((item, index) => (
                    <div className="listIssues"
                         key={index}
                         style={{display: this.getIssueFilter(this.state.issueFilter, item.state)}}>
                        <div className="issueInformation">
                            <span className="issueComments"><img src="/img/comment.png" style={{width:15, marginRight:'5px', marginBottom:'-4px', opacity:0.5}}></img>{item.comments}</span>
                            <div className="issueTitle">
                                <img src={this.state.iconPath} className="issueIcon" />
                                <form action={'/issues/' + item.number} method="POST">
                                    <input type="hidden" name="_token" value={this.props.csrf_token} />
                                    <input type="hidden" name="issue" value={JSON.stringify(item)} />
                                    <button type="submit" className="entryLink pointer"> {item.title}</button>
                                </form>
                            </div>
                            <div className="issueItemInfo">
                                {item.id} opened <TimeAgo date={item.created_at} /> by <span className={"issueUser"}>{item.user.login}</span>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

if (document.getElementById('listOfIssues')) {
    const component = document.getElementById('listOfIssues');
    const props = Object.assign({}, component.dataset);
    ReactDOM.render(<ListOfIssues {...props} />, component);
}
